# Sujet 1 : Pour un numérique soutenable, « des bonnes intentions [...] ce n'est pas suffisant »

###### Référence

- *Pour un numérique soutenable, « des bonnes intentions [...] ce n'est pas suffisant »*, par Sébastien Gavois, in NextInpact, 06/01/2021. [nextinpact.com/article/45093/pour-numerique-soutenable-bonnes-intentions-ce-nest-pas-suffisant](nextinpact.com/article/45093/pour-numerique-soutenable-bonnes-intentions-ce-nest-pas-suffisant).

##### Question
> Mobilisez les concepts liés à la raison computationnelle et au capitalisme de surveillance pour commenter la position de l'Arcep.



### Réponse

L'Arcep est un acronyme désignant l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse. Comme rappelé sur son site internet (https://www.arcep.fr/larcep/nos-missions.html) :

> L’Arcep est une autorité administrative indépendante (AAI). Elle assure la régulation des secteurs des communications électroniques et des postes, au nom de l’Etat, mais en toute indépendance par rapport au pouvoir politique et aux acteurs économiques.

Dans cette brève réponse au sujet, nous allons voir comment les trois grands thèmes évoquées dans l'article en question par l'Arcep concernant le numérique et la stratégie bas carbone entrent en résonance avec deux concepts étudiés à l'occasion des ateliers théoriques de l'enseignement WE01 ce semestre.

Dans un premier temps, nous allons mettre en perspective le concept de **raison computationelle** [élaboré par Bruno Bachimont](https://bibliotheque.utc.fr/EXPLOITATION/doc/ALOES/0209266/sens-de-la-technique-le) à partir des [travaux de Jack Goody](https://bibliotheque.utc.fr/EXPLOITATION/doc/ALOES/0166375/raison-graphique-la) avec l'idée évoquée de pouvoir "Améliorer la capacité de pilotage de l'empreinte environnementale du numérique par les pouvoirs publics". En effet, cette "capacité de *pilotage*" n'est permise que par les potentialités du calcul que nous offre les ordinateurs. On retrouve ainsi l'idée de Bruno Bachimont selon laquelle l'avènement des ordinateurs dans la société ont fait apparaître de nouvelles formes de représentation de la connaissance et ainsi de la raison, au travers de ce qu'il a appellé la **raison computationnelle**. Le numérique est devenu un outil calculatoire qui transforme nos manières de penser, devenant le milieu même de notre pensée. Le pilotage d'une empreinte numérique du numérique suppose des outils tels que des programmes, l'automatisation de processus, et l'évaluation de données. Cette ambition de l'Arcep est donc une illustration du phénomène de raison computationnelle.

Dans un second temps, nous allons aborder les liens entre les deux autres thèmes soulevés par l'Arcep et le concept de **capitalisme de surveillance** [développé par Christophe Masutti](https://cfeditions.com/masutti/). Ces deux autre thèmes sont celui de la volonté de pouvoir "Intégrer l'enjeu environnemental dans les actions de régulation de l'Arcep", et de pouvoir "Renforcer les incitations des acteurs économiques, acteurs privés, publics et consommateurs". Nous mettrons ici en perspective cet élan vers une *automatisation de processus de traitement de l'information* qui découlerait de ce genre de pratique, se basant sur des indicateurs qui pourraient être calculés par des nouveaux logiciels d'évaluation environnementale du numérique et qui serviraient de guide voire de mécanisme régulateur à l'Arcep. Or, Christophe Masutti révèle dans ses écrits qu'*à partir du moment où est mis en place un processus d'automatisation du traitement de l'information appraît la surveillance*. Celle-ci se basant sur des logiques de **stéréotypage** et de **quantification**, on pourrait alors se retrouver dans une société abusivement structurée par cette surveillance pouvant aller jusqu'à influer sur nos comportements et créant ainsi une société stéréotypée et liberticide.

L'Arcep pourrait ainsi prendre du recul sur le phénomène de mise en science, en projet, en programme et en indicateurs du processus d'évaluation de l'impact environnemental, afin d'éviter de tomber dans des travers "techno-optimistes" ou trop rigides.
